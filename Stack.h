#include<iostream>

using namespace std;

template <class T>

class Stack
{
private:
    T* stackPtr;                      
    const int size;                   
    int top = 0;                       
public:
    Stack(int = 1);                  
    Stack(const Stack<T>&);          
    ~Stack();                         

    void push(const T&);
    T pop();
    T* getPtr() const;
    int getStackSize() const;
    void showStack();
    const T& Peek(int) const; 
    int getCount() const;
};


template <typename T>
Stack<T>::Stack(int size) : size(size)
{
    stackPtr = new T[size];
    
}


template <typename T>
Stack<T>::Stack(const Stack<T>& otherStack) : size(otherStack.getStackSize())
{
    stackPtr = new T[size];
   top = otherStack.getCount();

    for (int i = 0; i < top; i++)
        stackPtr[i] = otherStack.getPtr()[i];
}


template <typename T>
Stack<T>::~Stack()
{
    delete[] stackPtr;
}


template <typename T>
void Stack<T>::push(const T& value)
{  
    if (top < size)
    {       
        stackPtr[top++] = value;
    }                                        
    else {
        cout << "Stack overflow!!!" << endl;
    }     
}

template <typename T>
T Stack<T>::pop()
{
    if (top > 0)
    {
        cout << "\nElement � " << top << ": " << Peek(1) << " removed" << endl;
       return stackPtr[--top];
    }
    else {
         cout << "Stack underflow!!!" << endl;
         return stackPtr[top];
    }
     
}

template <typename T>
T* Stack<T>::getPtr() const
{
    return stackPtr;
}


template <typename T>
int Stack<T>::getStackSize() const
{
    return size;
}


template <typename T>
void Stack<T>::showStack()
{
    cout << "\ntop: ";
    for (int i = top - 1; i >= 0; i--)
        cout << "\t" << stackPtr[i] << endl;
}


template <typename T>
int Stack<T>::getCount() const
{
    cout << "\nCount stack elements: " << top << endl;;
    return top;
}


template <class T>
const T& Stack<T>::Peek(int element) const
{
    if (element <= top)
    {
        return stackPtr[top - element];
    }
    else
    {
        cout << "Element is not" << endl;
        return stackPtr[top];
    }
    
}






