#include<iostream>
#include"Stack.h"

template <typename T>
 void PushStack(Stack<T> &stack)
{
    int i = 0;
    T value;

    while (i++ < stack.getStackSize())
    {
        cout << "Input value: ";
        cin >>  value;
        stack.push(value);
    }
    cout << endl;
}

int main()
{
	setlocale(LC_ALL, "ru");

    Stack<float> stackFloat(5);

    PushStack(stackFloat);

    stackFloat.push(2.33f);
    
    stackFloat.showStack();

    stackFloat.Peek(2);

    stackFloat.pop();

    stackFloat.showStack();

    stackFloat.push(2.33f);

    stackFloat.showStack();

    stackFloat.pop();
    
   Stack<float> stackFloat2(stackFloat);

    stackFloat2.showStack();
   

	return 0;
}